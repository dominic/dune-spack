from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneFunctions(dune_spack_module(name="dune-functions",
                                      git="https://gitlab.dune-project.org/staging/dune-functions.git",
                                      versions=["master", "2.6"],
                                      depends=["dune-localfunctions", "dune-grid", "dune-istl", "dune-typetree"],
                                      )
                    ):
    pass
