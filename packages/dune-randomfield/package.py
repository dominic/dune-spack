from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneRandomfield(dune_spack_module(name="dune-randomfield",
                                        depends=["dune-common"],
                                        git="https://gitlab.dune-project.org/dominic/dune-randomfield.git",
                                        versions=['master', '2.6'],
                                        )
                      ):
    depends_on("fftw")
    depends_on("hdf5+mpi")
