from spack import *


def dune_spack_module(name,
                      git=None,
                      versions=["master"],
                      homepage='https://www.dune-project.org',
                      depends=[],
                      suggests=[],
                      ):
    """ Set up a base class for a Spack-Dune module.
    This is done through a function to apply some defaults
    and to do tedious stuff once. Currently, this includes:
    * Make modules depend on matching versions of upstream modules.
    """
    if suggests:
        raise NotImplementedError("Dune module suggestions not yet implemented!")

    # Rename some variables that shadow spack directives. The alternative to doing
    # this would be to have less intuitive argument names on this function.
    git_url = git
    del git
    homepage_url = homepage
    del homepage

    class DuneModule(CMakePackage):
        # The following line is tricking spack into thinking that this
        # local object resides in the spack module of the given dune module.
        # This is necessary to have spack directives work correctly.
        # Ugly, I know.
        __module__ = "spack.pkg.dune.{}".format(name)

        # The homepage field is necessary information, so we default it to dune-project.org
        homepage = homepage_url

        depends_on('cmake@3.1.0:', type='build')

        def cmake_args(self):
            return []
        
        # Define all given versions in spack
        for v in versions:
            if v == "master":
                version(v, git=git_url, branch="master")
            else:
                version(v, git=git_url, branch="releases/{}".format(v))

        # Have each version depend on matching upstream Dune modules
        for dep in depends:
            for v in versions:
                depends_on("{}@{}".format(dep, v), when="@{}".format(v))

    return DuneModule



class DuneCommon(dune_spack_module("dune-common",
                                   git='https://gitlab.dune-project.org/core/dune-common.git',
                                   versions=["master", "2.6"],
                                   )
                 ):
    depends_on('mpi')
    
    variant('gmp', default=False, description="Whether to build multiprecision support into dune-common")
    depends_on('gmp', when="+gmp")
