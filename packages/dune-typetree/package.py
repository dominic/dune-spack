from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneTypetree(dune_spack_module(name="dune-typetree",
                                     git="https://gitlab.dune-project.org/staging/dune-typetree.git",
                                     versions=["master", "2.6"],                                     
                                     depends=["dune-common"],
                                     )
                   ):
    pass
