from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DunePdelab(dune_spack_module(name="dune-pdelab",
                                   git="https://gitlab.dune-project.org/pdelab/dune-pdelab.git",
                                   depends=["dune-grid", "dune-localfunctions", "dune-istl", "dune-typetree", "dune-functions"],
                                   versions=["master", "2.6"],
                                   )
                 ):
    pass
