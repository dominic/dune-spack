from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneGrid(dune_spack_module(name="dune-grid",
                                 git="https://gitlab.dune-project.org/core/dune-grid.git",
                                 versions=["master", "2.6"],
                                 depends=["dune-geometry"],
                                 )
               ):
    variant("uggrid", default=False, description="Whether to depend on dune-uggrid")
    depends_on("dune-uggrid", when="+uggrid")
