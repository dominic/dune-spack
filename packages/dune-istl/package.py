from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneIstl(dune_spack_module(name="dune-istl",
                                 git='https://gitlab.dune-project.org/core/dune-istl.git',
                                 versions=["master", "2.6"],
                                 depends=["dune-common"],
                                 )
               ):
    variant("superlu", default=True, description="Whether to build SuperLU bindings for dune-istl")
    depends_on("superlu", when="+superlu")

    variant("suite-sparse", default=False, description="Whether to build UMFPack bindings for dune-istl")
    depends_on("suite-sparse", when="+suite-sparse")
