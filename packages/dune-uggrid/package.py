from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneUggrid(dune_spack_module(name="dune-uggrid",
                                   depends=["dune-common"],
                                   git="https://gitlab.dune-project.org/staging/dune-uggrid.git",
                                   versions=['master', '2.6'],
                                   )
                 ):
    pass
