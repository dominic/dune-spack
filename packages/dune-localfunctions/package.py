from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneLocalfunctions(dune_spack_module(name="dune-localfunctions",
                                           git="https://gitlab.dune-project.org/core/dune-localfunctions.git",
                                           versions=["master", "2.6"],
                                           depends=["dune-geometry"],
                                           )
                         ):
    pass
