from spack import *


class DuneFuncep(CMakePackage):
    homepage="www.dune-project.org"
    version("master", git="https://parcomp-git.iwr.uni-heidelberg.de/Teaching/dune-funcep.git", branch="master")

    depends_on("dune-pdelab@2.6")
    depends_on("dune-randomfield@2.6")
    depends_on("dune-grid@2.6+uggrid")
    depends_on("hdf5+mpi")
