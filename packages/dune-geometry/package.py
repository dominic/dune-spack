from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneGeometry(dune_spack_module(name="dune-geometry",
                                     git="https://gitlab.dune-project.org/core/dune-geometry.git",
                                     versions=["master", "2.6"],
                                     depends=["dune-common"],
                                     )
                   ):
    pass
