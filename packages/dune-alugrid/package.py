from spack import *
from spack.pkg.dune.dune_common import dune_spack_module


class DuneAlugrid(dune_spack_module(name="dune-alugrid",
                                    depends=['dune-grid'],
                                    git="https://gitlab.dune-project.org/extensions/dune-alugrid.git",
                                    versions=['master', '2.6'],
                                    )
                  ):
    pass
