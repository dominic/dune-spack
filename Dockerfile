FROM debian:latest
MAINTAINER dominic.kempf@iwr.uni-heidelberg.de

RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  build-essential \
  ca-certificates \
  git             \
  procps          \
  python          \
  python-dev      \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/spack/spack.git
