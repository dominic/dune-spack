#!/bin/bash

set -e

for package in $(ls ./packages)
do
    echo "Trying the spec for package ${package}"
    spack spec $package
done
