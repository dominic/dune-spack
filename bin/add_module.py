#!/usr/bin/env python

#
# A script that reads a dune.module file and generates a spack module, that can
# then be extended with what ever additional logic you might want to implement.
#
# TODO list:
# * Implement parsing of version constraints from dune.module files
# * Pass maintainer field to the spack package
# * Implement suggestions
# * Extract the git field though git remote
#

import os
import re
import sys


def add_module(path):
    modfile = os.path.join(path, "dune.module")
    
    # Gather arguments to the call to dune_spack_module from
    # reading the dune.module file.
    args = {}
    for line in open(modfile, "r"):
        k, v = parse_line(line)
        if k:
            args[k] = v

    # Hardcode the available versions for now.
    args["versions"] = ["master", "2.6"]
    args["git"] = "https://please-fill-this.git"

    # Add the directory for this module
    cwd = os.getcwd()
    if os.path.split(cwd)[1] == "dune-spack":
        newdir = os.path.join(cwd, "packages", args["name"])
    elif os.path.split(cwd)[1] == "bin":
        newdir = os.path.join(os.path.split(cwd)[0], "packages", args["name"])
    else:
        raise ValueError("The add_module script should be called from the dune_spack root directory!")

    if os.path.isdir(newdir):
        raise ValueError("The module has already been added to the Spack repository")
    os.makedirs(newdir)

    # Write the package.py file
    packagefile = os.path.join(newdir, "package.py")
    with open(packagefile, "w") as f:
        f.write("from spack import *\n")
        f.write("from spack.pkg.dune.dune_common import dune_spack_module\n\n\n")
        
        part = "class {}(".format(camelify(args["name"]))
        ident1 = " " * len(part)
        part = "{}dune_spack_module(".format(part)
        ident2 = " " * len(part)
        f.write(part)
        for k, v in args.items():
            f.write("{}={},\n{}".format(k, stringify(v), ident2))
        f.write(")\n{}):\n    pass".format(ident1))


def parse_line(line):
    """ A poor mans parser for dune.module file lines """
    def get_property(prop):
        return re.match("{}: (.*)".format(prop), line)
    
    # Get match objects for lines
    module = get_property("Module")
    depends = get_property("Depends")

    if module:
        return "name", module.groups()[0]
    elif depends:
        return "depends", tuple(depends.groups()[0].split())
    else:
        print("Did not understand line: {}".format(line))
        return None, None


def camelify(s):
    return "".join(part.title() for part in s.split("-"))


def stringify(x):
    if isinstance(x, str):
        return '"{}"'.format(x)
    else:
        return x


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise ValueError("Wrong number of arguments, should be one")

    add_module(sys.argv[1])
